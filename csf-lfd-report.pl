#!/usr/bin/perl
# -*- mode: cperl; cperl-indent-level: 4; cperl-continued-statement-offset: 2 -*-

# CSF/LFD Report
# Summarizes the day's activity from /var/log/lfd.log. If threshold values are
# exceeded, then the report is sent in an email. If not, then no email is sent.
# Threshold values are listed under "Constants - Thresholds." Change them
# there, not within the routines.
# Requirements:    zgrep, Date::Calc, Regexp::Common
# Usage: csf-lfd-report.pl
# Created on: 6 July 2014
# Last updated on: 26 Dec 2020
# Created by: Terry Roy
# See csf-lfd-report -h for usage, copyright, etc.

use strict;
use warnings;
use 5.016;
use utf8;
use autodie qw( :all );
use Carp;
use Cwd;
#use Data::Dumper qw( Dumper ); #only used when troubleshooting
use Date::Calc qw(Add_Delta_Days Delta_Days Month_to_Text Today);
use English qw( -no_match_vars );
use Getopt::Long;
use Pod::Usage;
use Regexp::Common qw /net/;
use Readonly;

# Version
our $VERSION = 2.00;

# Constants
Readonly my $DAY_PAD     => 10;                       # Used in date_string sub
Readonly my $MAX_ARGS    =>  4;                       # Maximum number of arguments
Readonly my $MAX_OFF     => 14;                       # Maximum offset
Readonly my $LOG_CURRENT => '/var/log/lfd.log';
Readonly my $LOG_ARCHIVE => '/var/log/lfd.log.1.gz';

# Constants - Thresholds
Readonly my $TH_0                 => 0; # used for thresholds that should
                                        # always be 0 - errors, unknown and integrity.
Readonly my $TH_cnt_portscans     => 10;
Readonly my $TH_cnt_netblocks     => 5;
Readonly my $TH_cnt_ruleblocks    => 100;
Readonly my $TH_ssh_logins        => 2;
Readonly my $TH_user_process      => 0;

# Default Options
my $detail           = 'medium';            # default
my $offset           = '1';                 # default

# Scalars
my $cnt_args         = @ARGV;               # number of arguments
my $cnt_blocks       = 0;                   # count of all blocks
my $cnt_blockerrs    = 0;                   # count of errors in IP blocks
my $cnt_portscans    = 0;                   # count of port scans
my $cnt_netblocks    = 0;                   # count of class C network blocks
my $cnt_ruleblocks   = 0;                   # count of custom blocks
my $cnt_errors       = 0;                   # count of error messages
my $cnt_integrity    = 0;                   # count of files changed
my $version_cmd      = '/usr/sbin/csf -v';  # returns current version
my $sep1             = q{####};             # used in report
my $sep2             = q{##};               # used in report
my $indent1          = q{    };             # four spaces
my $indent2          = q{      };           # six spaces
my $print_report     = 0;                   # default to not print report
my $script           = $0;

my ( $cur_version, $date_report, $date_str );

# regex
my $date_re;    # qr created in SUB date_string, used in PROCESS
my $ip = "$RE{net}{IPv4}|$RE{net}{IPv6}"; # As string to insert. No compilation needed.

# arrays
my ( @blocks, @blocks_err, @errors,
    @integrity, @log_lines, @unknown
);

# hashes
my (
    %blocks_portscan, %blocks_net, %blocks_rules, %cnt_blocktypes, %errorlist,
    %ip_info, %ip_ctry_info, %ssh_logins, %user_process
);

# subs
sub archive_lines;      # test lfd.log.1.gz & capture lines if date matches
sub date_string;        # returns date in format of log file date
sub print_blocks;       # print IP blocks
sub print_other;        # print report
sub process_blocks;     # process IP blocks
sub process_ipinfo;     # process IP host and country
sub process_loglines;   # process log lines
sub process_messages;   # process non-block log lines
sub valid_opts;         # validate options

#-------------Start------------#
## Exit with help for too many arguments
if ( $MAX_ARGS < $cnt_args ) {
  pod2usage(
    -msg => "Too many arguments in the command line. Script exiting....\n",
    -verbose => 0
  );
}

# Validate and assign option values if given
# otherwise set defaults
if ( 0 < $cnt_args ) {
    valid_opts;
}

# Generate date string for match against log line
date_string;

# Current csf version
$cur_version = qx{$version_cmd};

# Get lines matching date_str from current log
open my $fh, '<', $LOG_CURRENT;
while (<$fh>) {
    chomp;
    if ( m/^$date_re/ms ) { push @log_lines, $_ };
}
close $fh;

# Test lfd.log.1.gz archive for lines matching date.
# Add any existing lines to @log_lines.
archive_lines($date_str);

# Process @log_lines array for matches
process_loglines;

# IP blocks need further processing
if ( scalar @blocks > 0 ) {
    process_blocks;
}

# Ip info processed after it's extracted from @blocks.
if ( keys %ip_info > 0 ) {
    process_ipinfo;
}

# Threshold tests to determine whether to print or not.
if ( scalar(@errors) > $TH_0 ) { $print_report = 1 };
if ( $cnt_blockerrs > $TH_0 ) { $print_report = 1 };
if ( scalar(@unknown) > $TH_0 ) { $print_report = 1 };
if ( scalar keys(%ssh_logins) > $TH_ssh_logins ) { $print_report = 1 };
if ( scalar keys(%user_process) > $TH_user_process ) { $print_report = 1 };
if ( scalar(@integrity) > $TH_0 ) { $print_report = 1 };
if ( $cnt_portscans > $TH_cnt_portscans ) { $print_report = 2 };
if ( $cnt_netblocks > $TH_cnt_netblocks ) { $print_report = 2 };
if ( $cnt_ruleblocks > $TH_cnt_ruleblocks ) { $print_report = 2 };

# Print report
if ( $print_report > 0 ) {

  # Header lines
  printf "%s\n", "CSF & LFD Report for $date_str";
  printf "%s\n", 'Prepared: ' . localtime;
  printf "%s\n", "Current csf version: $cur_version";

  # Print errors/possible issues first
  # Always print. Not dependent on detail level.
  # Error messages
  if ( scalar(@errors) > $TH_0 ) {
      printf "\n%s\t%s\t%s\n", "$sep1", 'Error Messages', "$sep1";
      printf "%s\n", 'These errors were logged. You might want to investigate.';
      foreach my $line (@errors) {
          $errorlist{$line}++;
      }
      printf "%-25s\t%s\n", 'Error', '# times';
      foreach my $key ( sort keys %errorlist ) {
          printf "%-5s %d\n", "$key", "$errorlist{$key}";
      }
  }

  if ( $cnt_blockerrs > $TH_0 ) {
      printf "\n%s%s%s%s\n", "$indent1", "$sep2", 'Blocks not handled - Please fix script', "$sep2";
      foreach my $line (@blocks_err) {
          printf "\t%s\n", "$line";
      }
  }

  # Print @unknown
  if ( scalar(@unknown) > $TH_0 ) {
      printf "\n%s\t%s\t%s\n", "$sep1", 'Other Not Handled', "$sep1";
      printf "These lines weren\'t processed.\n";
      printf "The script may need to be modified.\n\n";
      foreach my $line (@unknown) {
          printf "%s\n", "$line";
      }
  }

  # Print IP Blocks and info
  print_blocks;

  # Print ssh logins, user processing
  print_other;
}

#######################################################################
##                          Subroutines                             ##
#######################################################################

# Test archive for lines matching date
sub archive_lines {
    my $date = shift;

    # system grep command
    my $grep_str = "zgrep '$date' $LOG_ARCHIVE";

    # Use system grep to see if date exists in archive file
    my @grep_results = `$grep_str`;
    my $grep_cnt     = scalar @grep_results;
    if ( $grep_cnt > 0 ) {
        foreach my $line (@grep_results) {
            push @log_lines, $line;
        }
    }
    return;
}

# create date_str & date_re
sub date_string {
    # Variables
    my ( $year, $mon, $day, @today );
    # Get current date
    @today = Today();
    # Calculate report date
    ( $year, $mon, $day ) = Add_Delta_Days( @today, "-$offset" );

    # Convert month number to text
    $mon = substr Month_to_Text($mon), '0', '3';

    # Pad the date if necessary.  lfd doesn't use 0N format for days 1-9
    if ( $day < $DAY_PAD ) {
        $day = " $day";
    }
    # Assemble date strings
    $date_re  = $mon . '\s+' . $day . '\s';
    $date_str = "$mon $day";
    $date_report = "$mon $day, $year";

    return;
}

sub print_blocks {
# Blocks
    if ( $cnt_blocks > 0 && $print_report == 2 ) {

        # header
        printf "\n%s\t%s\t%s\n", "$sep1", 'Blocks', "$sep1";
        printf "%-15s%5d\n", 'Total Blocks', $cnt_blocks;

        printf "%-15s%5d\n", 'Port Scans', $cnt_portscans;
        printf "%-15s%5d\n", 'Class C Net', $cnt_netblocks;
        printf "%-15s%5d\n", 'Rules', $cnt_ruleblocks;

        # Port Scans
        if ( $detail ne 'low' ) {
            if ( $cnt_portscans > 0 ) {
                printf "\n%s%s%s\n", "$sep2", 'Port Scans', "$sep2";
                # FIX - need for medium and high.
                # FIX counts
                foreach my $key ( sort keys %blocks_portscan ) {
                    printf "%-25s%10d\n", "$key", "$blocks_portscan{$key}";
                }
            }

          # Netblocks
          if ( $cnt_netblocks > $TH_cnt_netblocks ) {
              printf "\n%s%s%s\n", "$sep2", 'Class C Net Blocks', "$sep2";
              # FIX - need for medium and high
              foreach my $key ( sort keys %blocks_net ) {
                  printf "%s%-20s%d\n", "$indent2", "$key", "$blocks_net{$key}";
              }
          }
        }

        # IP blocks
        if ( $cnt_ruleblocks > $TH_cnt_ruleblocks ) {
            printf "\n%s\t%s\t%s\n", "$sep2", 'Rule Blocks', "$sep2";
            printf "%-25s%10d\n", 'Total Rule Blocks', "$cnt_ruleblocks";
            foreach my $key ( sort keys %blocks_rules ) {
                printf "%-25s%10d\n", "$key", "$cnt_blocktypes{$key}";
                # FIX - need for medium and high.
                foreach my $key1 ( sort keys %{ $blocks_rules{$key} } ) {
                    if ( ( $blocks_rules{$key}{$key1} > 1 && $detail eq 'medium' ) || $detail eq 'high' ) {
                        printf "%s%-25s%10d\n", "$indent1", "$key1", "$blocks_rules{$key}{$key1}";
                    }
                }
            }
        }
        if ( $detail eq 'high' ) {
            # IP Country Info
            printf "\n%s%s%s\n", "$sep2", 'IP Country Info', "$sep2";
            my $cnt_ctry = scalar keys %ip_ctry_info;
            printf "%-25s%d\n", 'Total countries', "$cnt_ctry";
            foreach my $key ( sort keys %ip_ctry_info ) {
                printf "%s\n", "$key";
                foreach my $key1 ( sort keys %{ $ip_ctry_info{$key} } ) {
                    printf "%s%-10s\n", $indent1, "$key1";
                }
            }
        }
    }

    return;
}

sub print_other {

    # SSH Logins
    if ( scalar keys(%ssh_logins) > $TH_ssh_logins ) {
        printf "\n%s\t%s\t%s\n", "$sep1", 'Successful SSH Logins', "$sep1";
        foreach my $key ( sort keys %ssh_logins ) {
            printf "%s\n", "$key";
            foreach my $key1 ( sort keys %{ $ssh_logins{$key} } ) {
                printf "%s%-25s%2s\n", "$indent1", "$key1", "$ssh_logins{$key}{$key1}";
            }
        }
    }
    # User Processing
    if ( scalar keys(%user_process) > $TH_user_process ) {
        printf "\n%s\t%s\t%s\n", "$sep1", 'User processing', "$sep1";
        foreach my $key ( sort keys %user_process ) {
            printf "%s\n", "$key";
        }
    }

    return;
}

# Further process IP blocks.  Extracts IP, country and host info
sub process_blocks {
    my ( $key, $key1, $text );
    foreach my $line (@blocks) {
        # Classify Port Scans, Netblocks and Rules bocks
        if ( $line =~
            m/Port\sScan\sdetected\sfrom\s($ip)\s(Unknown|([[:upper:]]{2}\/.+\/\S+))(?:\s|\.)/msx )
        {
            $cnt_portscans++;
            $key  = $1;    # IP
            $text = $2;    # country and host info
            $blocks_portscan{$key}++;
            if ( $text ne 'Unknown' ) { $ip_info{$text}{$key}++ };
        }
        elsif ( $line =~ m/NETBLOCK\s(\S+)\s/msx ) {
            $cnt_netblocks++;
            $key = $1;     # IP
                           # no country info on netblocks
            $blocks_net{$key}++;
        }
        # Single IP blocks
       elsif ( $line =~ m/(\S+)\s.+\s($ip)\s(.+):\s/msx ) {
            $cnt_ruleblocks++;
            $key  = $1;    # block name
            $key1 = $2;    # IP
            $text = $3;    # country and host info
            $blocks_rules{$key}{$key1}++;
            $ip_info{$text}{$key1}++;
            $cnt_blocktypes{$key}++;
        }
       elsif ( $line =~ m/PERMBLOCK\s($ip)\s([[:upper:]]{2}\/.+\/\S+)/msx ) {
            $cnt_ruleblocks++;
            $key  = 'PERMBLOCK';    # block name
            $key1 = $1;    # IP
            $text = $2;    # country and host info
            $blocks_rules{$key}{$key1}++;
            $ip_info{$text}{$key1}++;
            $cnt_blocktypes{$key}++;
        }
        else {
            $cnt_blockerrs++;
            push @blocks_err, $line;
        }
    }
    return;
}

# Process IP info - country and host
sub process_ipinfo {

    my ( $ctry_abb, $ctry, $host );
    foreach my $key ( sort keys %ip_info ) {
        #test for missing country info
        my $cnt = $key =~ tr/\//\//;

        if ( $cnt == 2 ) {
          ( $ctry_abb, $ctry, $host ) = split m/\//msx, $key, '3';
           if ( $host =~ m/^[-]$/msx ) { $host = 'no rDNS'};
          $ip_ctry_info{$ctry}{$host}++;
        }
        else {
          $ctry_abb ='N/A';
          $ctry = 'Unknown';
          $host = $key;
        }
    }
    return;
}

# Process the log lines for matches
sub process_loglines {

    foreach my $line (@log_lines) {
        chomp $line;

        # skip messages related to starting and stopping lfd
        next if ( $line =~ m/TERM$/msx );
        next if ( $line =~ m/Global\s(?:Allow|Deny|Ignore)\s-\sretrieved/msx );
        next if ( $line =~ m/daemon\s(?:started|stopped).*$/msx );
        next if ( $line =~ m/Unzipped\sBlocklist/msx );
        next if ( $line =~ m/LF_APACHE_ERRPORT\:\sSet\sto/msx );
        next if ( $line =~ m/\*Account\sModification\*\sEmail\ssent/msx );
        next if ( $line =~ m/:\s\[.+\],\[0\],/msx );
        next if ( $line =~ m/\[.+_LOG.+\[1\]/msx );
        next if ( $line =~ m/(?:load|switch|Watch|Track|Process|Extract|Repopulat|Retriev|Reopen)(?:ed|ing)/msx );
        next if ( $line =~ m/(?:Log\sScanner|IPv6\sEnabled|Country\sCode\s(?:Lookups|Filters))\.\.\./msx );
        next if ( $line =~ m/(Incoming|Outgoing)\sIP.+removed/msx );
        next if ( $line =~ m/\*System\sIntegrity\*\shas\sdetected\smodified\sfile/msx );
        next if ( $line =~ m/IPSET\:\sloading\sset\snew/msx );

        # split the line and remove some info we don't need
        my ( $server_info, $message ) = split /: /ms, $line, 2;
        $message =~ s/[*\[\]()]//msxg;

        # process the message
        if ( $message =~ m/Blocked\sin\scsf/msx ) {
            $cnt_blocks++;
            push @blocks, $message;
        }
        else {
            process_messages($message);
        }
    }
    return;
}

# Classify messages and extract info
sub process_messages {
    my $msg = shift;
    my ( $key, $key1, $text );

    # Process lines - matches are tested in order of potential frequency
    # i.e. block removed usually occurs more frequently than any other
    # SSH logins
    if ( $msg =~
        m{SSH\slogin\sfrom\s
        ($ip)
        \sinto\sthe\s
        (\S+)
        \saccount\susing\s
        (\S+)
        \sauthentication}msx
      )
    {
        $key1 = $1 . '   Type: ' . $3;    # ip address & type
        $text = $2;                       # account name
        if ( $msg =~ m/-\signored/msx ) { $text = $text . ' - ignored' };
        $key  = $text;
        $ssh_logins{$key}{$key1}++;
    }
    # user processing
    elsif (
        $msg =~ m/User Processing.*User:(\S+)\sTime:\d+\s(EXE:\S+\sCMD:.+$)/msx )
    {
        $key  = $1;                       # user
        $text = $2;                       # exe & cmd
        $user_process{$key}++;
    }
    # Deal with error messages
    elsif ( $msg =~ m/(Error|Unable)/msx ) {
        $cnt_errors++;
        push @errors, $msg;
    }
    # Not handled
    else {
        push @unknown, $msg;
    }
    return;
}
## Validate options
## Usage: valid_opts;
sub valid_opts {

    my ( $help, $man, $ver );

    eval {
        GetOptions(
            'd|detail:s'    => \$detail,
            'o|offset:i'    => \$offset,
            'v|version'     => \$ver,
            'h|help'        => \$help,
            'm|manual'      => \$man,
        );
    } or pod2usage( -verbose => 1 );

    ## Check detail
    if ( $detail !~ m/(?:low|medium|high)/msx ) {
        pod2usage(
            -msg => "Invalid detail value: $detail. Use low, medium or high.\nExiting....\n",
            -verbose => 1,
            -exitval => 2
        );
    }

    ## Check offset - Out of the box, lfd keeps two logs, the current week
    ## and the previous week, so a maximum of 14 days worth of logs would
    ## be available on the day before rotation.
    if ( $offset > $MAX_OFF || $offset < 0 ) {
        pod2usage(
            -msg => "Invalid offset: $offset. This must be an integer from 0 to 14.\nExiting....\n",
            -verbose => 1,
            -exitval => 2
        );
    }

    ## Version
    if ( $ver ) {
        my ($path, $file) = split /\/([^\/]+)$/msx, $script;
        print "$file ver. $VERSION located in $path/\n";
        exit;
    }

    ## Help
    if ( $help ) {pod2usage( -verbose => 0 ) };

    ## Manual
    if ( $man ) {
      pod2usage(
          -noperldoc => 1,
          -verbose   => 2
      )
    }

    return;
}

__END__

=pod

=head1 NAME

csf-lfd-report.pl - Summarize lfd log from Configserver Security & Firewall (csf)

=head1 USAGE

csf-lfd-report.pl  [B<-o> I<N>] [B<-d> I<low|medium|high>]

Examples:

 csf-lfd-report.pl -o 3 -d high

For explanations of the above, see the EXAMPLES section of the manual.

B<Help Options:>

B<-h>, B<--help>     Show the help information and exit.

B<-m>, B<--manual>   Show the manual and exit.

B<-v>, B<--version>  Show the version number and exit.

=head1 DESCRIPTION

This Perl script summarizes the information in /var/log/lfd.log for
ConfigServer Security & Firewall (https://www.configserver.com/cp/csf.html).

It was initially developed on a Debian Wheezy server with Perl v5.14.2.

=head2 Normal Usage

With no options given, i.e. executing F<csf-lfd-report.pl> will generate a
report for the previous day with medium detail. This is useful when setting
up a cron job.

The script does not write to any file or alter existing files in any
way.

Two options are available to:
  * limit or expand the output, B<-d|--detail> option, and
  * report on a date other than the previous day, B<-o|--offset> option.

=head3 Detail

The levels of detail available are low, medium and high.

The low level of detail excludes IP-specific information for blocks and
all country/host information.

The medium level of detail includes IP-specific information for IPs
that were blocked more than once.

The high level of detail includes all IP-specific information as well
as country/host information.

=head3 Offset

Offset is the number of days from today. For example, an offset of three
will give you a report from three days ago. Out of the box, lfd keeps
two logs, one for the current week (F</var/log/lfd.log>) and one for the
previous week F</var/log/lfd.log.1.gz>, so a maximum of 14 days worth of
logs would be available. If a day isn't available, a blank report is
returned.

=head1 REQUIRED ARGUMENTS

There are no required arguments.

=head1 OPTIONS

=head2 General

Options may not be bundled.

=head2 Help Options

=over 4

=item B<-h>, B<--help>

Show the brief help information and exit.

=item B<-m>, B<--manual>

Read the manual, with examples and exit.

=item B<-v>, B<--version>

Show the version number and exit.

=back

=head2 Offset option

=over 4

=item B<-o>, B<--offset>

Default - Processes yesterday's log entries. Equivalent to
setting '-o 1'.

For the current day, use '-o 0'.

Maximum value is 14.

=item B<-d> I<value>, B<--detail> I<value>

Default - medium detail

Values available are 'low' (no IP detail), 'medium' (detail for
IPs seen more than once) or 'high' (all IP/Country/Host information).

=back

=head1 EXAMPLES

  The following are examples of this script:

  csf-lfd-report.pl

  Run with all defaults:
    * Data from previous day
    * Detail is low
        * No host/country info
        *IPs with more than 1 block

  csf-lfd-report.pl -d low -o 5
    * Data from 5 days ago
    * Detail is low
        * No host/country info
        * No IPs

  csf-lfd-report.pl -d high -o 0
    * Data from current day
    * Detail is high
        * Country/host information
        * All IPs

  csf-lfd-report.pl -h  Read help
  csf-lfd-report.pl -m  Read manual
  csf-lfd-report.pl -v  Show version

=head1 DIAGNOSTICS

Here, %s represents information that will vary. B<(F)> indicates a fatal
message and the script will exit. B<(W)> indicates a warning message and
the script will continue.

=head2 Processing errors

ADD INFO

=over 4

=item B<Unable to open file referenced.>

B<(F)> The file name that couldn't be opened will print out below the message.

 File name: %s

=item B<Unable to close file %s>

B<(W)> The script was unable to close the file it had opened.

=back

=head2 Command line options and arguments errors

The following messages relate to command line options and arguments.

=over 4

=item B<Unknown option in the command line>

B<(F)> A usage message will print. Check the manual for a more extensive
explanation of valid options.

=item B<Invalid option value: %s. Use 'low', 'medium' or 'high'.>

B<(F)> Check the manual for valid option values. for the B<-d>, B<--detail> option.

=item B<Too many arguments in the command line>

B<(F)> Maximum number of arguemnts is two.

=item B<Invalid offset: %s. This must be an integer from 0 to 14.>

B<(F)> The option value must be a number between 0 and 14.

=back

=head1 EXIT STATUS

The script will exit with 0 if successful.

=head1 CONFIGURATION

Configuration is done entirely through command line options.  There is
no associated configuration file.

=head1 DEPENDENCIES

This script requires two modules, Date::Calc and Regexp::Common,
as well as the system's zgrep.

=head1 INCOMPATIBILITIES

No known incompatibilities.

=head1 BUGS AND LIMITATIONS

The minimum required version is Perl v5.11.0.

The script assumes the standard lfd log files are present.

=back

Please report any bugs or feature requests through the web interface
at L<Bitbucket|https://bitbucket.org/troubleshooter/csf-lfd-report/src/master/>.

=head1 NOTES

=over 4

=item 1. Generic servers

This script was developed using the generic version of csf.

=back

=head1 AUTHOR

Terry Roy
--
https://bitbucket.org/troubleshooter/csf-lfd-report/src/master/

=head1 LICENSE AND COPYRIGHT

Copyright 2014-to present Terry Roy

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See http://dev.perl.org/licenses/artistic.html

=head1 DISCLAIMER OF WARRANTY

This software is provided "as is" and you use at your own risk.

See http://dev.perl.org/licenses/artistic.html

=cut
